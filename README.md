## Installation

- docker-compose up -d
- docker exec -ti <service> bash
- cp .env.example .env
- php artisan key:generate
- composer install
- php artisan migrate

##Testing
- php artisan db:seed
- php artisan test

## Assumptions for further development

- Add __Payable__ model that will have polymorhpic relationships with e.g. currencies and use it instead of the amount 
in __Transaction__ model. 
- More tests
- Forbid to make transactions between two same wallets (add rules)
- Add error checking
- Add ordering
- More abstraction
- Better navigation
- Security, e.g. now can post not yours wallet to transaction form
