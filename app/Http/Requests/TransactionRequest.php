<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TransactionRequest
 *
 * @package App\Http\Requests
 * @author Sergei Randin svr@svr.lv
 */
class TransactionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from_wallet' => 'required|numeric',
            'to_wallet' => 'required|numeric',
            'amount' => 'required|numeric|max:4294967295',
            'reference' => 'required'
        ];
    }
}
