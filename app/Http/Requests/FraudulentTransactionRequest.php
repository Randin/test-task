<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FraudulentTransactionRequest
 *
 * @package App\Http\Requests
 * @author Sergei Randin svr@svr.lv
 */
class FraudulentTransactionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fraud' => 'required',
        ];
    }
}
