<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class WalletRequest
 *
 * @package App\Http\Requests
 * @author Sergei Randin svr@svr.lv
 */
class WalletRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|min:3'
        ];
    }
}
