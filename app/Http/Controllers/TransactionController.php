<?php

namespace App\Http\Controllers;

use App\Http\Requests\FraudulentTransactionRequest;
use App\Http\Requests\TransactionRequest;
use App\Facades\Cashier;

/**
 * Class TransactionController
 *
 * @package App\Http\Controllers
 * @author Sergei Randin svr@svr.lv
 */
class TransactionController extends Controller
{
    private $transactionRepository;
    private $walletRepository;

    /**
     * Transaction controller constructor
     */
    public function __construct()
    {
        $this->transactionRepository = Cashier::getTransactionRepository();
        $this->walletRepository = Cashier::getWalletRepository();
    }

    public function create()
    {
        $fromWallets = $this->walletRepository->getUserWallets();
        $allWallets = $this->walletRepository->all();

        return view('transactions.create', compact(['fromWallets', 'allWallets']));
    }

    public function store(TransactionRequest $request)
    {
        $this->transactionRepository->create($request);

        return redirect()->back()->with('status', 'Transaction has been added');
    }

    public function update(FraudulentTransactionRequest $request, $id)
    {
        $this->transactionRepository->markFraudulent($id, $request);

        return redirect()->back()->with('status', 'Transaction marked fraudulent');
    }

    public function destroy($id)
    {
        $this->transactionRepository->delete($id);

        return redirect()->back()->with('status', 'Transaction has been deleted');
    }
}
