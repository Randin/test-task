<?php

namespace App\Http\Controllers;

use App\Repositories\WalletRepositoryInterface;
use Illuminate\Contracts\Support\Renderable;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 * @author Sergei Randin svr@svr.lv
 */
class HomeController extends Controller
{
    private $walletRepository;

    /**
     * Home controller constructor
     *
     * @param $walletRepository
     */
    public function __construct(WalletRepositoryInterface $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */

    public function index()
    {
        $wallets = $this->walletRepository->getUserWallets();

        return view('home', compact('wallets'));
    }
}
