<?php

namespace App\Http\Controllers;

use App\Facades\Cashier;
use App\Http\Requests\WalletRequest;

/**
 * Class WalletController
 *
 * @package App\Http\Controllers
 * @author Sergei Randin svr@svr.lv
 */
class WalletController extends Controller
{
    protected $walletRepository;

    /**
     * Wallet controller constructor
     */
    public function __construct()
    {
        $this->walletRepository = Cashier::getWalletRepository();
    }

    public function create()
    {
        return view('wallets.create');
    }

    public function store(WalletRequest $request)
    {
        $this->walletRepository->create($request);

        return redirect()->back()->with('status', 'Wallet has been added');
    }

    public function edit($id)
    {
        $wallet = $this->walletRepository->get($id);

        return view('wallets.edit', compact('wallet'));
    }

    public function update(WalletRequest $request, $id)
    {
        $this->walletRepository->update($id, $request);

        return redirect()->back()->with('status', 'Wallet has been renamed');
    }

    public function show($id)
    {
        $wallet = Cashier::prepareSums($id);

        return view('wallets.show', compact('wallet'));
    }

    public function destroy($id)
    {
        $this->walletRepository->delete($id);

        return redirect()->back()->with('status', 'Wallet has been deleted');
    }
}
