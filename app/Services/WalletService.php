<?php
namespace App\Services;

use App\Models\Wallet;
use App\Repositories\TransactionRepositoryInterface;
use App\Repositories\WalletRepositoryInterface;

/**
 * Class WalletService
 *
 * Contains Wallet Business Logic
 *
 * @package App\Services
 * @author Sergei Randin svr@svr.lv
 */
class WalletService extends Service
{
    private $walletRepository;
    private $transactionRepository;

    /**
     * WalletService constructor.
     *
     * @param WalletRepositoryInterface $walletRepository
     * @param TransactionRepositoryInterface $transactionRepository
     */
    public function __construct(
        WalletRepositoryInterface $walletRepository,
        TransactionRepositoryInterface $transactionRepository
    ){
        $this->walletRepository = $walletRepository;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Prepare wallet data
     *
     * @param $id
     * @return Wallet
     */
    public function prepareSums($id) : Wallet
    {
        $wallet = $this->walletRepository->get($id);
        $wallet->transactions = $wallet->deposits->merge($wallet->withdrawals);
        $wallet->sumDeposits = $wallet->deposits->sum('amount');
        $wallet->sumWithdrawals = $wallet->withdrawals->sum('amount');
        $wallet->sumTransactions = $wallet->sumDeposits - $wallet->sumWithdrawals;

        return $wallet;
    }

    /**
     * Returns the repository
     *
     * @return WalletRepositoryInterface
     */
    public function getWalletRepository() : WalletRepositoryInterface
    {
        return $this->walletRepository;
    }

    /**
     * Returns the repository
     *
     * @return WalletRepositoryInterface
     */
    public function getTransactionRepository() : TransactionRepositoryInterface
    {
        return $this->transactionRepository;
    }
}
