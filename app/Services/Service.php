<?php
namespace App\Services;

/**
 * Class Service
 *
 * Abstract class for future abstraction, e.g. getRepository method
 *
 * @package App\Services
 * @author Sergei Randin svr@svr.lv
 */
abstract class Service
{

}
