<?php
namespace App\Repositories;

use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Class TransactionRepository
 *
 * @package App\Repositories
 * @author Sergei Randin svr@svr.lv
 */
class TransactionRepository implements TransactionRepositoryInterface
{
    /**
     * Create transaction
     *
     * @param $postData
     * @return Transaction
     */
    public function create($postData) : Transaction
    {
        return Transaction::create([
                'wallet_from' => $postData->input('from_wallet'),
                'wallet_to' => $postData->input('to_wallet'),
                'amount' => $postData->input('amount'),
                'reference' => $postData->input('reference'),
            ]
        );
    }

    /**
     * Mark transaction fraudulent
     *
     * @param $id
     * @param $postData
     * @return int
     */
    public function markFraudulent($id, $postData) : int
    {
        return Transaction::whereId($id)->update($postData->only('fraud'));
    }

    /**
     * Delete transaction
     *
     * @param $id
     * @return bool
     */
    public function delete($id) : bool
    {
        $transaction = Transaction::findOrFail($id);

        return $transaction->delete();
    }
}
