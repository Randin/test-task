<?php
namespace App\Repositories;

use App\Http\Requests\WalletRequest;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Class WalletRepository
 *
 * @package App\Repositories
 * @author Sergei Randin svr@svr.lv
 */
class WalletRepository implements WalletRepositoryInterface
{
    /**
     * Get's a wallet by it's ID
     *
     * @param int
     * @return Wallet
     */
    public function get($walletId) : Wallet
    {
        return Wallet::findOrFail($walletId);
    }

    /**
     * Get's all wallets.
     *
     * @return Collection
     */
    public function all() : Collection
    {
        return Wallet::get();
    }

    /**
     * Get wallets that belong to authenticated user
     *
     * @return Collection
     */
    public function getUserWallets() : Collection
    {
        return Wallet::where('user_id', Auth::id())->get();
    }

    /**
     * Deletes a wallet.
     *
     * @param int
     * @return bool
     */
    public function delete($walletId) : bool
    {
        $wallet = Wallet::findOrFail($walletId);
        return $wallet->delete();
    }

    /**
     * Updates a wallet.
     *
     * @param int $walletId
     * @param WalletRequest $postData
     * @return bool
     */
    public function update($walletId, WalletRequest $postData) : bool
    {
        return Wallet::whereId($walletId)->update($postData->only('name'));
    }

    /**
     * Create new wallet.
     *
     * @param WalletRequest $postData
     * @return Wallet
     */
    public function create(WalletRequest $postData) : Wallet
    {
        return Wallet::create([
                'name' => $postData->input('name'),
                'user_id' => Auth::id(),
            ]
        );
    }
}
