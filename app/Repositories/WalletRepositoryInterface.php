<?php
namespace App\Repositories;

use App\Http\Requests\WalletRequest;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface WalletRepositoryInterface
 *
 * @package App\Repositories
 * @author Sergei Randin svr@svr.lv
 */
interface WalletRepositoryInterface
{
    /**
     * Get's a wallet by it's ID
     *
     * @param int $walletId
     * @return Wallet
     */
    public function get($walletId) : Wallet;

    /**
     * Get's all wallets.
     *
     * @return Collection
     */
    public function all() : Collection;

    /**
     * Get wallets that belong to authenticated user
     *
     * @return Collection
     */
    public function getUserWallets(): Collection;

    /**
     * Create new wallet.
     *
     * @param WalletRequest $data
     * @return Wallet
     */
    public function create(WalletRequest $data) : Wallet;

    /**
     * Deletes a wallet.
     *
     * @param int
     * @return bool
     */
    public function delete($walletId) : bool;

    /**
     * Updates a wallet.
     *
     * @param int
     * @param WalletRequest $postData
     * @return bool
     */
    public function update($wallet_id, WalletRequest $postData) : bool;
}
