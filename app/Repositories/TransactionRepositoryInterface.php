<?php

namespace App\Repositories;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface TransactionRepositoryInterface
 *
 * @package App\Repositories
 * @author Sergei Randin svr@svr.lv
 */
interface TransactionRepositoryInterface
{
    /**
     * Create transaction
     *
     * @param $postData
     * @return Transaction
     */
    public function create($postData): Transaction;

    /**
     * Mark transaction fraudulent
     *
     * @param $id
     * @param $postData
     * @return int
     */
    public function markFraudulent($id, $postData): int;


    /**
     * Delete transaction
     *
     * @param $id
     * @return bool
     */
    public function delete($id): bool;
}
