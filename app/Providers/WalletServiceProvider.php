<?php

namespace App\Providers;

use App\Repositories\TransactionRepository;
use App\Repositories\WalletRepository;
use Illuminate\Support\ServiceProvider;
use App\Services\WalletService;

/**
 * Class WalletServiceProvider
 *
 * @package App\Providers
 * @author Sergei Randin svr@svr.lv
 */
class WalletServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind('cashier', function(){
           return new WalletService(new WalletRepository(), new TransactionRepository());
        });
    }
}
