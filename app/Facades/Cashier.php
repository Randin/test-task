<?php
namespace App\Facades;

use App\Services\WalletService;
use Illuminate\Support\Facades\Facade;

/**
 * Class Wallet
 *
 * Facade for @see WalletService
 *
 * @package App\Facades
 */
class Cashier extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cashier';
    }
}
