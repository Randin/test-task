<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaction
 *
 * @package App\Models
 * @author Sergei Randin svr@svr.lv
 */
class Transaction extends Model
{
    use SoftDeletes;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'wallet_from',
        'wallet_to',
        'amount',
        'reference'
    ];

    /**
     * Get the hidden attributes for the model.
     *
     * @return array
     */
    public function getHidden()
    {
        return [
            'updated_at',
            'created_at',
            'deleted_at',
        ];
    }

    /**
     * Wallet relationships for deposits
     *
     * @return BelongsTo
     */
    public function from()
    {
        return $this->belongsTo(Wallet::class, 'wallet_from');
    }


    /**
     * Wallet relationships for withdrawals
     *
     * @return BelongsTo
     */
    public function to()
    {
        return $this->belongsTo(Wallet::class, 'wallet_to');
    }
}
