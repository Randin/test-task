<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Wallet
 *
 * @package App\Models
 * @author Sergei Randin svr@svr.lv
 */
class Wallet extends Model
{
    use SoftDeletes;

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id'
    ];

    /**
     * Get the hidden attributes for the model.
     *
     * @return array
     */
    public function getHidden()
    {
        return [
            'updated_at',
            'created_at',
            'deleted_at',
        ];
    }

    /**
     * Get the deposits for the wallet.
     */
    public function deposits()
    {
        return $this->hasMany(Transaction::class, 'wallet_to');
    }

    /**
     * Get the withdrawals for the wallet.
     */
    public function withdrawals()
    {
        return $this->hasMany(Transaction::class, 'wallet_from');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
