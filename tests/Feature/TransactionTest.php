<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    /**
     * Test if transactions created
     *
     * @return void
     */
    public function testUserCreation()
    {
        $this->assertDatabaseCount('transactions', 50);
    }
}
