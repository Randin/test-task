<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * Test if users created
     *
     * @return void
     */
    public function testUserCreation()
    {
        $this->assertDatabaseCount('users', 200);
    }
}
