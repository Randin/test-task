<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WalletTest extends TestCase
{
    /**
     * Test if wallets created
     *
     * @return void
     */
    public function testWalletCreation()
    {
        $this->assertDatabaseCount('wallets', 150);
    }
}
