@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>Your wallets <a href="{{ route('wallet.create') }}" class="label label-default">{{ __('Add new') }}</a></h3>

                    <ul class="list-group">
                    @forelse($wallets as $wallet)
                            <li class="list-group-item">
                                <h3 class="text-center">
                                    <a href="{{ route('wallet.show', $wallet->id) }}">{{ $wallet->name }}</a>
                                </h3>
                                <hr />
                                <div class="btn-group" role="group" aria-label="...">
                                    <a class="btn btn-primary" href="{{ route('wallet.edit', $wallet->id) }}">{{ __('Rename') }}</a>
                                    <form action="{{ route('wallet.destroy', $wallet->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">{{ __('Delete') }}</button>
                                    </form>
                                </div>
                            </li>
                    @empty
                            <li class="list-group-item">You have no wallets, please create one</li>
                    @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
