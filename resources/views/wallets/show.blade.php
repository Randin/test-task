@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ $wallet->name }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <h3>Transactions <a href="{{ route('transaction.create') }}" class="label label-default">{{ __('Add new') }}</a></h3>

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="row">Total</th>
                                    <th scope="row">Deposits</th>
                                    <th scope="row">{{ $wallet->sumDeposits }}</th>
                                    <th scope="row">Withdrawals</th>
                                    <th scope="row">{{ $wallet->sumWithdrawals }}</th>
                                    <th></th>
                                    <th scope="row">Balance</th>
                                    <th scope="row">{{ $wallet->sumTransactions }}</th>
                                </tr>
                                <tr>
                                    <th scope="col">Transaction ID</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">FROM</th>
                                    <th scope="col">TO</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Reference</th>
                                    <th scope="col">Fraudulent</th>
                                    <th scope="col">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($wallet->transactions as $transaction)
                                    <tr>
                                        <th scope="row">#{{ $transaction->id }}</th>
                                        <td>{{ $transaction->created_at }}</td>
                                        <td>{{ $transaction->from->name }}</td>
                                        <td>{{ $transaction->to->name }}</td>
                                        <td>{{ $transaction->amount }}</td>
                                        <td>{{ $transaction->reference }}</td>
                                        <td>
                                            {{ $transaction->fraud }}
                                            @if(!$transaction->fraud)
                                                <form method="POST" action="{{ route('transaction.update', $transaction->id) }}">
                                                    @csrf
                                                    @method('PATCH')
                                                    <input type="hidden" value="1" name="fraud">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Mark') }}
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                        <td>
                                            <form action="{{ route('transaction.destroy', $transaction->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger" type="submit">{{ __('Delete') }}</button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <th scope="row">You</th>
                                        <td>do</td>
                                        <td>not</td>
                                        <td>have</td>
                                        <td>any</td>
                                        <td>transactions</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
