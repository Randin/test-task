@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create new Transaction') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('transaction.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="from_wallet" class="col-md-4 col-form-label text-md-right">{{ __('From') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('from_wallet') is-invalid @enderror" id="from_wallet" name="from_wallet" required>
                                        <option>Choose wallet</option>
                                        @forelse($fromWallets as $fromWallet)
                                        <option value="{{ $fromWallet->id }}">{{ $fromWallet->name }}</option>
                                        @empty

                                        @endforelse
                                    </select>
                                    @error('from_wallet')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <label for="to_wallet" class="col-md-4 col-form-label text-md-right">{{ __('To') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('to_wallet') is-invalid @enderror" id="to_wallet" name="to_wallet" required>
                                        <option>Choose wallet</option>
                                        @forelse($allWallets as $allWallet)
                                            <option value="{{ $allWallet->id }}">{{ $allWallet->name }}</option>
                                        @empty

                                        @endforelse
                                    </select>
                                    @error('to_wallet')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <label for="from_wallet" class="col-md-4 col-form-label text-md-right">{{ __('Amount') }}</label>
                                <div class="col-md-6">
                                    <input id="amount" type="number" class="form-control @error('amount') is-invalid @enderror" name="amount" value="{{ old('amount') }}" required autocomplete="amount" autofocus>
                                    @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <label for="from_wallet" class="col-md-4 col-form-label text-md-right">{{ __('Reference') }}</label>
                                <div class="col-md-6">
                                    <input id="reference" type="text" class="form-control @error('reference') is-invalid @enderror" name="reference" value="{{ old('reference') }}" required autocomplete="reference" autofocus>
                                    @error('reference')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
