<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Transaction;
use App\Models\Wallet;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'wallet_from' => factory(Wallet::class),
        'wallet_to' => factory(Wallet::class),
        'amount' => $faker->numberBetween(1, 1000),
        'reference' => $faker->sentence
    ];
});
