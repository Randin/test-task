<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->softDeletes();
            $table->foreignId('wallet_from');
            $table->foreignId('wallet_to');
            $table->unsignedInteger('amount');
            $table->string('reference');
            $table->boolean('fraud')->default(false);

            $table->foreign('wallet_from')
                ->references('id')
                ->on('wallets')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('wallet_to')
                ->references('id')
                ->on('wallets')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
